<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored ="false" %>
<html>
    <head>
        <title>Update ${currentStudent.firstName} ${currentStudent.lastName}</title>
    </head>
    <body>
        <h2>
            Update ${currentStudent.firstName} ${currentStudent.lastName}:
        </h2>
        <form action="updateStudentInDb" method="POST">
                    <input type="hidden" name="id" value= ${currentStudent.id}>
                    <p>First name:</p>
                    </ br>
                    <p>Current : ${currentStudent.firstName}</p>
                    <p><input type="text" name="firstName"></p>
                    </ br>
                    <p>Last name:</p>
                    </ br>
                    <p>Current : ${currentStudent.lastName}</p>
                    <p><input type="text" name="lastName"></p>
                    </ br>
                    <p>Age:</p>
                    </ br>
                    <p>Current : ${currentStudent.age}</p>
                    <p><input type="number" name="age"></p>
            <input type="submit" name="Submit"/>
        </form>
    </body>
</html>