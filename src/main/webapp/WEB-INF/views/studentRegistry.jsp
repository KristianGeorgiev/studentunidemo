<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page isELIgnored ="false" %>
<html>
    <head>
        <title>Student Registry</title>
    </head>
    <body>
        <h1>Student Registry!</h1>

        <button type="button" onclick="location='delete_student'">Delete Student</button>

        <button type="button" onclick="location='addStudent'">Add Student</button>

        <button type="button" onclick="location='updateStudent'">Update Student</button>

        <table border="1">
            <thead>
                <tr>
                    <td> ID </td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Age</td>
                </tr>
            </thead>
            <c:if test="${not empty students}">
                <tbody>
                    <c:forEach var="s" items="${students}">
                        <tr>
                            <td>${s.id}</td>
                            <td>${s.firstName}</td>
                            <td>${s.lastName}</td>
                            <td>${s.age}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </c:if>
        </table>

    </body>
</html>