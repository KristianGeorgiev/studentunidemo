<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored ="false" %>
<html>
    <head>
        <title>Delete student</title>
    </head>
    <body>
        <h2>
            Delete student by ID:
        </h2>
        <form action="deleteStudentFromDS">

            Insert ID<input type="text" name="id">

            <input type="submit" name="Delete"/>
        </form>
        <table border="1">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                    <td>Age</td>
                </tr>
            </thead>
                <c:if test="${not empty studentsList}">
                    <tbody>
                        <c:forEach var="s" items="${studentsList}">
                            <tr>
                                <td>${s.id}</td>
                                <td>${s.firstName}</td>
                                <td>${s.lastName}</td>
                                <td>${s.age}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
    </body>
</html>