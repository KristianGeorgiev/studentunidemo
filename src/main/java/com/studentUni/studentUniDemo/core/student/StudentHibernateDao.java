package com.studentUni.studentUniDemo.core.student;

import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentHibernateDao implements StudentDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List<Student> list() {
        return getSession().createCriteria(Student.class).list();
    }

    public Student get(final Long id) {
        return (Student) getSession().createCriteria(Student.class).add(Restrictions.eq("id", id)).uniqueResult();
    }

    public void save(final Student student) {
        getSession().save(student);

    }

    public void update(final Student student) {
        getSession().update(student);
    }

    public void delete(Student student) {
        getSession().delete(student);
    }

    public Session getSession() {
        try {
            return  sessionFactory.getCurrentSession();
        }catch (HibernateException e){
           return sessionFactory.openSession();
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
