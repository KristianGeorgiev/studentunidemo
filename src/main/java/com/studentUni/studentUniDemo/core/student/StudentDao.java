package com.studentUni.studentUniDemo.core.student;

import java.util.List;

public interface StudentDao {

    Student get(final Long id);

    List<Student> list();

    void save(final Student student);

    void delete(Student student);

    void update(Student student);
}
