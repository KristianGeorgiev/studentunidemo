package com.studentUni.studentUniDemo.core.student;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "student")
public class Student implements Serializable , Cloneable{

    private static final long serialVersionUID = -7049957706738879274L;

    @Id
    @SequenceGenerator(name="student_id_seq" , sequenceName="student_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="student_id_seq")
    @Column(name = "student_id")
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "age", nullable = false)
    private int age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
