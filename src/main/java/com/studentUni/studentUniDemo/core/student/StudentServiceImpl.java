package com.studentUni.studentUniDemo.core.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Transactional
    public boolean addStudent(Student student) {

       studentDao.save(student);

        return true;
    }

    @Transactional
    public boolean deleteStudent(Student student) {
        Student persistedStudent = studentDao.get(student.getId());
        studentDao.delete(persistedStudent);

        return true;
    }

    public List<Student> getStudents() {
        return studentDao.list();
    }

    @Transactional
    public boolean update(Student student) {
        Student persistedEntity = studentDao.get(student.getId());

        if (!persistedEntity.getFirstName().equalsIgnoreCase(student.getFirstName())) {
            persistedEntity.setFirstName(student.getFirstName());
        }

        if (!persistedEntity.getLastName().equalsIgnoreCase(student.getLastName())) {
            persistedEntity.setLastName(student.getLastName());
        }

        if (persistedEntity.getAge() != student.getAge()) {
            persistedEntity.setAge(student.getAge());
        }

        studentDao.update(persistedEntity);

        return false;
    }

    public Student getStudent (Student student) {
        return studentDao.get(student.getId());
    }
}
