package com.studentUni.studentUniDemo.core.student;

import java.util.List;

public interface StudentService {

    boolean addStudent(Student student);

    boolean deleteStudent(Student student);

    List<Student> getStudents();

    boolean update(Student student);

    Student getStudent(Student student);
}
