package com.studentUni.studentUniDemo.controllers;

import com.studentUni.studentUniDemo.core.student.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    @Autowired
    private StudentService service;

    @RequestMapping(value="/" , method = RequestMethod.GET)
    public String homePageRender(Model model) {
        model.addAttribute("students", service.getStudents());

        return "studentRegistry";
    }
}
