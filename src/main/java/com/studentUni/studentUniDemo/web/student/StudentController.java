package com.studentUni.studentUniDemo.web.student;

import com.studentUni.studentUniDemo.core.student.Student;
import com.studentUni.studentUniDemo.core.student.StudentService;
import com.studentUni.studentUniDemo.core.student.StudentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentValidator studentValidator;

    @RequestMapping(value = "/studentRegistry", method = RequestMethod.GET)
    public String getStudent(Model model){

        model.addAttribute("students", studentService.getStudents());
        return "studentRegistry";
    }

    @RequestMapping(value="/addStudent", method = RequestMethod.GET)
    public String addStudent() {

        return "addStudent";
    }


    @RequestMapping(value = "/addStudentSave", method = RequestMethod.POST)
    public String addStudent(Model model, @ModelAttribute(value="student") Student student) {

        studentService.addStudent(student);

        model.addAttribute("students", studentService.getStudents());

        return "studentRegistry";
    }

    @RequestMapping(value="/delete_student", method = RequestMethod.GET)
    public String deleteStudent(Model model) {

        model.addAttribute("studentsList", studentService.getStudents());

        return "delete_student";

    }

    @RequestMapping(value = "/deleteStudentFromDS", method = RequestMethod.GET)
    public String deleteStudentFromDB(@ModelAttribute(value = "student") Student student) {

        studentService.deleteStudent(student);

        return "redirect:/studentRegistry";
    }

    @RequestMapping(value = "/updateStudent")
    public String updateStudentPageRender(Model model) {

        model.addAttribute("studentsList", studentService.getStudents());

        return "updateStudent";
    }

    @RequestMapping(value = "/showSelectedStudentFromDb")
    public String showSelectedStudent(Model model, @ModelAttribute(value = "student") Student student) {

        model.addAttribute("currentStudent", studentService.getStudent(student));

        return "updateStudentData";
    }

    @RequestMapping(value = "/updateStudentInDb")
    public String updateStudentInDb(@ModelAttribute(value = "student") Student student) {

        studentService.update(student);

        return "redirect:/studentRegistry";
    }

}
