<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Add student</title>
    </head>
    <body>
        <h2>
            Add new student:
        </h2>
        <form action="addStudentSave" method="POST">
            <table>
                <tr>
                    <td>First name:</td>
                    <td><input type="text" name="firstName"></td>
                    <td>Last name:</td>
                    <td><input type="text" name="lastName"></td>
                    <td>Age:</td>
                    <td><input type="number" name="age"></td>
                </tr>
            </table>
            <input type="submit" name="Submit"/>
        </form>
    </body>
</html>